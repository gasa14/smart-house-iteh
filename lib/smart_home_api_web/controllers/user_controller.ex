defmodule SmartHomeApiWeb.UserController do
  use SmartHomeApiWeb, :controller

  alias SmartHomeApi.Users
  alias SmartHomeApi.Users.User

  action_fallback SmartHomeApiWeb.FallbackController

  def index(conn, _params) do
    users = Users.list_users()
    render(conn, :index, users: users)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Users.create_user(user_params),
        {:ok, token, _claims} <- SmartHomeApiWeb.Auth.Guardian.encode_and_sign(user) do
      conn
      |> put_status(:created)
      |> render("user_token.json", %{user: user, token: token})
    end
  end

  def show(conn, %{"id" => id}) do
    user = Users.get_user!(id)
    render(conn, :show, user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Users.get_user!(id)

    with {:ok, %User{} = user} <- Users.update_user(user, user_params) do
      render(conn, :show, user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Users.get_user!(id)

    with {:ok, %User{}} <- Users.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
