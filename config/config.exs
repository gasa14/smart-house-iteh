# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :smart_home_api,
  ecto_repos: [SmartHomeApi.Repo],
  generators: [timestamp_type: :utc_datetime, binary_id: true]

# Configures the endpoint
config :smart_home_api, SmartHomeApiWeb.Endpoint,
  url: [host: "localhost"],
  adapter: Phoenix.Endpoint.Cowboy2Adapter,
  render_errors: [
    formats: [json: SmartHomeApiWeb.ErrorJSON],
    layout: false
  ],
  pubsub_server: SmartHomeApi.PubSub,
  live_view: [signing_salt: "0EMQz5Pq"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :smart_home_api, SmartHomeApiWeb.Auth.Guardian,
    issuer: "smart_home_api",
    secret_key: "l9fX5kRO/9H2t1LqiZY+R81qxpVUKApunmgtoki7cqrXcBz2CLDQKkjLt4K13R5O"

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
